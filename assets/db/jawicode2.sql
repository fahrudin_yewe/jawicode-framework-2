/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : jawicode2

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 03/05/2021 13:30:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori`  (
  `id_kat` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `for_modul` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES (2, 'FAKIR', 'FKR', 'ASNAF');
INSERT INTO `kategori` VALUES (3, 'MISKIN', 'MSK', 'ASNAF');
INSERT INTO `kategori` VALUES (4, 'PEDULI KONSUMTIF', '2', 'PROGRAM');
INSERT INTO `kategori` VALUES (5, 'PEDULI BENCANA', '2', 'PROGRAM');
INSERT INTO `kategori` VALUES (6, 'PEDULI DIFABEL', '2', 'PROGRAM');
INSERT INTO `kategori` VALUES (7, 'PEDULI MODAL', '2', 'PROGRAM');
INSERT INTO `kategori` VALUES (8, 'SANTUNAN DUAFA', '3', 'KEGIATAN');

-- ----------------------------
-- Table structure for master_access
-- ----------------------------
DROP TABLE IF EXISTS `master_access`;
CREATE TABLE `master_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_access` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_access
-- ----------------------------
INSERT INTO `master_access` VALUES (1, 'M_USERS', 'MENU USER', '0000-00-00 00:00:00', 0);
INSERT INTO `master_access` VALUES (4, 'M_LAPORAN', 'MENU LAPORAN', NULL, NULL);
INSERT INTO `master_access` VALUES (5, 'M_SY_CONFIG', 'MENU SISTEM', '0000-00-00 00:00:00', 0);
INSERT INTO `master_access` VALUES (16, 'M_SY_CONFIG', '', '2019-06-12 07:48:03', 1);
INSERT INTO `master_access` VALUES (46, 'M_PROPOSAL_H', '', '2019-12-06 20:33:24', 1);
INSERT INTO `master_access` VALUES (47, 'C_PROPOSAL', '', '2019-12-06 20:33:33', 1);
INSERT INTO `master_access` VALUES (48, 'R_PROPOSAL', '', '2019-12-06 20:33:41', 1);
INSERT INTO `master_access` VALUES (49, 'U_PROPOSAL', '', '2019-12-06 20:33:50', 1);
INSERT INTO `master_access` VALUES (50, 'D_PROPOSAL', '', '2019-12-06 20:33:57', 1);
INSERT INTO `master_access` VALUES (51, 'E_PROPOSAL', '', '2019-12-13 11:54:31', 1);
INSERT INTO `master_access` VALUES (56, 'M_M_ASNAF', '', '2020-01-16 16:40:31', 1);
INSERT INTO `master_access` VALUES (57, 'C_MASTER_ASNAF', '', '2020-01-16 16:40:42', 1);
INSERT INTO `master_access` VALUES (58, 'U_MASTER_ASNAF', '', '2020-01-16 16:40:49', 1);
INSERT INTO `master_access` VALUES (59, 'R_MASTER_ASNAF', '', '2020-01-16 16:40:58', 1);
INSERT INTO `master_access` VALUES (60, 'D_MASTER_ASNAF', '', '2020-01-16 16:41:04', 1);
INSERT INTO `master_access` VALUES (61, 'M_M_PROGRAM', '', '2020-01-16 17:31:17', 1);
INSERT INTO `master_access` VALUES (62, 'C_MASTER_PROGRAM', '', '2020-01-16 17:31:22', 1);
INSERT INTO `master_access` VALUES (63, 'R_MASTER_PROGRAM', '', '2020-01-16 17:31:28', 1);
INSERT INTO `master_access` VALUES (64, 'U_MASTER_PROGRAM', '', '2020-01-16 17:31:33', 1);
INSERT INTO `master_access` VALUES (65, 'D_MASTER_PROGRAM', '', '2020-01-16 17:31:39', 1);
INSERT INTO `master_access` VALUES (66, 'M_M_KEGIATAN', '', '2020-01-16 17:46:34', 1);
INSERT INTO `master_access` VALUES (67, 'R_MASTER_KEGIATAN', '', '2020-01-16 17:48:24', 1);
INSERT INTO `master_access` VALUES (68, 'C_MASTER_KEGIATAN', '', '2020-01-16 17:48:39', 1);
INSERT INTO `master_access` VALUES (69, 'U_MASTER_KEGIATAN', '', '2020-01-16 17:48:44', 1);
INSERT INTO `master_access` VALUES (70, 'D_MASTER_KEGIATAN', '', '2020-01-16 17:48:51', 1);
INSERT INTO `master_access` VALUES (71, 'M_M_MUZAKI', '', '2020-01-22 20:48:47', 1);
INSERT INTO `master_access` VALUES (72, 'C_MASTER_MUZAKI', '', '2020-01-22 20:48:55', 1);
INSERT INTO `master_access` VALUES (73, 'U_MASTER_MUZAKI', '', '2020-01-22 20:49:01', 1);
INSERT INTO `master_access` VALUES (74, 'D_MASTER_MUZAKI', '', '2020-01-22 20:49:07', 1);
INSERT INTO `master_access` VALUES (75, 'M_M_MUSTAHIK', '', '2020-01-26 09:20:20', 1);
INSERT INTO `master_access` VALUES (76, 'C_MASTER_MUSTAHIK', '', '2020-01-26 09:20:32', 1);
INSERT INTO `master_access` VALUES (77, 'U_MASTER_MUSTAHIK', '', '2020-01-26 09:20:39', 1);
INSERT INTO `master_access` VALUES (78, 'D_MASTER_MUSTAHIK', '', '2020-01-26 09:20:46', 1);
INSERT INTO `master_access` VALUES (79, 'R_MASTER_MUSTAHIK', '', '2020-01-26 09:20:55', 1);
INSERT INTO `master_access` VALUES (80, 'M_REALISASI_H', '', '2020-02-03 18:53:01', 1);
INSERT INTO `master_access` VALUES (81, 'C_REALISASI_HEADER', '', '2020-02-03 18:53:06', 1);
INSERT INTO `master_access` VALUES (82, 'R_REALISASI_HEADER', '', '2020-02-03 18:53:12', 1);
INSERT INTO `master_access` VALUES (83, 'U_REALISASI_HEADER', '', '2020-02-03 18:53:17', 1);
INSERT INTO `master_access` VALUES (84, 'D_REALISASI_HEADER', '', '2020-02-03 18:53:22', 1);
INSERT INTO `master_access` VALUES (85, 'M_ANGGARAN_H', '', '2020-02-03 19:40:15', 1);
INSERT INTO `master_access` VALUES (86, 'C_ANGGARAN_HEADER', '', '2020-02-03 19:40:21', 1);
INSERT INTO `master_access` VALUES (87, 'R_ANGGARAN_HEADER', '', '2020-02-03 19:40:27', 1);
INSERT INTO `master_access` VALUES (88, 'U_ANGGARAN_HEADER', '', '2020-02-03 19:40:32', 1);
INSERT INTO `master_access` VALUES (89, 'D_ANGGARAN_HEADER', '', '2020-02-03 19:40:37', 1);
INSERT INTO `master_access` VALUES (90, 'M_ANGGARAN_DETIL', '', '2020-02-03 19:44:30', 1);
INSERT INTO `master_access` VALUES (91, 'C_ANGGARAN_DETIL', '', '2020-02-03 19:45:42', 1);
INSERT INTO `master_access` VALUES (92, 'R_ANGGARAN_DETIL', '', '2020-02-03 19:45:47', 1);
INSERT INTO `master_access` VALUES (93, 'U_ANGGARAN_DETIL', '', '2020-02-03 19:45:52', 1);
INSERT INTO `master_access` VALUES (94, 'D_ANGGARAN_DETIL', '', '2020-02-03 19:45:57', 1);
INSERT INTO `master_access` VALUES (95, 'M_DISKET1', NULL, NULL, 1);
INSERT INTO `master_access` VALUES (96, 'M_DISPOSURVEY', NULL, NULL, 1);
INSERT INTO `master_access` VALUES (97, 'M_DISPORAPAT', NULL, NULL, 1);
INSERT INTO `master_access` VALUES (98, 'M_DISPOWAKA2', NULL, NULL, 1);
INSERT INTO `master_access` VALUES (99, 'M_DISPOPELAKSANA', NULL, NULL, 1);
INSERT INTO `master_access` VALUES (100, 'M_DISPOWAKA4', NULL, NULL, 1);
INSERT INTO `master_access` VALUES (101, 'M_MUTASI_H', '', '2020-03-04 06:02:49', 1);
INSERT INTO `master_access` VALUES (102, 'M_MUTASI_D', '', '2020-03-04 06:02:56', 1);

-- ----------------------------
-- Table structure for sy_config
-- ----------------------------
DROP TABLE IF EXISTS `sy_config`;
CREATE TABLE `sy_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `conf_val` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sy_config
-- ----------------------------
INSERT INTO `sy_config` VALUES (3, 'APP_NAME', 'Baznas Temanggung', '');
INSERT INTO `sy_config` VALUES (8, 'OPD_NAME', 'Baznas Temanggung', '');
INSERT INTO `sy_config` VALUES (9, 'LEFT_FOOTER', '<strong>Copyright</strong> Peternak Kode © 2019', '');
INSERT INTO `sy_config` VALUES (10, 'RIGHT_FOOTER', 'Baznas Temanggung', '');
INSERT INTO `sy_config` VALUES (11, 'APP_DESC', 'Aplikasi Pengelola Data Baznas Temanggung', '-');
INSERT INTO `sy_config` VALUES (12, 'OPD_ADDR', 'Jln. Barito 2, Sidotopo, Kota Magelang', '');
INSERT INTO `sy_config` VALUES (13, 'VISI_MISI', 'Bekerja Untuk Sesama', '');
INSERT INTO `sy_config` VALUES (14, 'APP_TELP', '0899999999', '');
INSERT INTO `sy_config` VALUES (15, 'APP_EMAIL', 'apayaemailnya@mail.com', '');
INSERT INTO `sy_config` VALUES (16, 'APP_FB', 'https://www.facebook.com', '');
INSERT INTO `sy_config` VALUES (17, 'APP_TWITTER', 'https://twitter.com', '');
INSERT INTO `sy_config` VALUES (18, 'APP_IG', 'https://instagram.com', '');
INSERT INTO `sy_config` VALUES (32, 'PENGHARGAAN_PENGAJAR_TITLE', 'Prestasi Pengajar', '');
INSERT INTO `sy_config` VALUES (33, 'PENGHARGAAN_PENGAJAR_DESC', 'Prestasi atas usaha keras pengajar kami membuahkan hasil dengan diraihnya beberapa penghargaan berikut.', '');
INSERT INTO `sy_config` VALUES (34, 'PENGHARGAAN_SISWA_TITLE', 'Prestasi Siswa', '');

-- ----------------------------
-- Table structure for user_access
-- ----------------------------
DROP TABLE IF EXISTS `user_access`;
CREATE TABLE `user_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NULL DEFAULT NULL,
  `kd_access` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_access` varbinary(100) NULL DEFAULT NULL,
  `is_allow` int(1) NULL DEFAULT NULL COMMENT '0=false,1=true',
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 179 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_access
-- ----------------------------
INSERT INTO `user_access` VALUES (5, 2, '1', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (8, 1, '1', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (9, 3, '5', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (10, 3, '1', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (11, 3, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (12, 4, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (13, 1, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (14, 1, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (15, 1, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (16, 1, '5', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (17, 1, '6', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (18, 3, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (19, 2, '5', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (20, 4, '5', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (21, 4, '6', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (22, 3, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (23, 4, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (24, 1, '7', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (25, 1, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (26, 1, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (27, 1, '10', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (28, 5, '10', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (29, 5, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (30, 2, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (31, 2, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (32, 1, '14', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (33, 2, '14', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (34, 1, '12', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (35, 2, '12', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (36, 1, '13', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (37, 1, '11', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (38, 5, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (39, 5, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (40, 2, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (41, 2, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (42, 3, '6', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (43, 3, '7', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (44, 3, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (45, 3, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (46, 3, '10', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (47, 3, '11', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (48, 3, '12', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (49, 3, '13', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (50, 4, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (51, 4, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (52, 4, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (53, 5, '15', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (54, 1, '15', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (55, 1, '16', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (56, 6, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (57, 6, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (58, 6, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (59, 6, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (60, 6, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (61, 6, '14', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (62, 6, '15', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (63, 5, '16', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (64, 5, '17', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (65, 5, '18', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (66, 5, '19', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (67, 5, '21', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (68, 6, '18', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (69, 1, '17', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (70, 1, '18', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (71, 1, '19', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (72, 1, '21', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (73, 1, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (74, 2, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (75, 6, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (76, 5, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (77, 5, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (78, 5, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (79, 2, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (80, 1, '23', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (81, 1, '24', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (82, 1, '25', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (83, 1, '26', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (84, 1, '27', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (85, 1, '28', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (86, 1, '29', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (87, 1, '30', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (88, 1, '31', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (89, 1, '32', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (90, 1, '33', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (91, 1, '34', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (92, 1, '35', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (93, 2, '24', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (94, 1, '36', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (95, 1, '37', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (96, 1, '38', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (97, 1, '39', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (98, 1, '40', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (99, 1, '41', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (100, 1, '42', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (101, 1, '43', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (102, 1, '44', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (103, 1, '45', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (104, 1, '46', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (105, 1, '50', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (106, 1, '49', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (107, 1, '48', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (108, 1, '47', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (109, 1, '51', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (110, 1, '52', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (111, 1, '53', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (112, 1, '54', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (113, 1, '55', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (114, 2, '56', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (115, 2, '57', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (116, 2, '58', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (117, 2, '59', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (118, 2, '60', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (119, 1, '56', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (120, 1, '57', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (121, 1, '58', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (122, 1, '59', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (123, 1, '60', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (124, 2, '61', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (125, 2, '62', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (126, 2, '63', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (127, 2, '64', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (128, 2, '65', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (129, 1, '61', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (130, 1, '62', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (131, 1, '63', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (132, 1, '64', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (133, 1, '65', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (134, 1, '66', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (135, 1, '67', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (136, 1, '68', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (137, 1, '69', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (138, 1, '70', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (139, 1, '71', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (140, 1, '72', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (141, 1, '73', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (142, 1, '74', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (143, 1, '75', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (144, 1, '76', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (145, 1, '77', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (146, 1, '78', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (147, 1, '79', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (148, 1, '80', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (149, 1, '81', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (150, 1, '82', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (151, 1, '83', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (152, 1, '84', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (153, 1, '85', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (154, 1, '86', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (155, 1, '87', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (156, 1, '88', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (157, 1, '89', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (158, 1, '90', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (159, 1, '91', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (160, 1, '92', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (161, 1, '93', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (162, 1, '94', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (163, 3, '95', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (164, 4, '96', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (165, 5, '98', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (166, 6, '99', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (167, 7, '100', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (168, 1, '95', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (169, 1, '96', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (170, 1, '97', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (171, 1, '98', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (172, 1, '99', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (173, 1, '100', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (174, 1, '101', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (175, 1, '102', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (176, 5, '97', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (177, 2, '46', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (178, 2, '85', NULL, 1, NULL);

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_group
-- ----------------------------
INSERT INTO `user_group` VALUES (1, 'Developer', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (2, 'Sekretaris', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (3, 'Ketua 1', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (4, 'Tim Survey', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (5, 'Wakil Ketua 2', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (6, 'Kepala Pelaksana', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (7, 'Wakil Ketua 4', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_group` int(11) NULL DEFAULT NULL COMMENT 'fk dari tabel user_group',
  `foto` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telp` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `note_1` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ip` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Developer', 'dev', '227edf7c86c02a44d17eec9aa5b30cd1', 'dev@email.com', 1, 'a4.jpg', '085643242654', 'full akses', 1, 1, '2018-03-13 03:06:55', '2020-04-03 09:50:11', '', '', '2019-08-27 20:12:45');
INSERT INTO `users` VALUES (2, 'Sekretaris', 'sekretaris', '6ad14ba9986e3615423dfca256d04e3f', 'fahrudinyewe@gmail.com', 2, 'a4.jpg', '085643242656', 'Verifikasi', 1, 1, '2018-03-13 03:06:55', '2019-08-18 14:25:27', '', '', '2019-08-27 20:12:45');
INSERT INTO `users` VALUES (3, 'Ketua 1', 'ketuasatu', '6ad14ba9986e3615423dfca256d04e3f', 'ketua1@mail.com', 3, '', '089636555456', '', 1, 0, '2020-02-27 05:31:56', '0000-00-00 00:00:00', '', '', NULL);
INSERT INTO `users` VALUES (4, 'Survey', 'survey', '6ad14ba9986e3615423dfca256d04e3f', 'survey@mail.com', 4, '', '089636555456', '', 1, 0, '2020-02-27 05:32:52', '0000-00-00 00:00:00', '', '', NULL);
INSERT INTO `users` VALUES (5, 'Wakil Ketua 2', 'wakildua', '6ad14ba9986e3615423dfca256d04e3f', 'wakildua@mail.com', 5, '', '089636555456', '', 1, 0, '2020-02-27 05:33:44', '0000-00-00 00:00:00', '', '', NULL);
INSERT INTO `users` VALUES (6, 'Ketua Pelaksana', 'pelaksana', '6ad14ba9986e3615423dfca256d04e3f', 'pelaksana@mail.com', 6, '', '089636555456', '', 1, 0, '2020-02-27 05:35:10', '0000-00-00 00:00:00', '', '', NULL);
INSERT INTO `users` VALUES (7, 'Wakil Ketua 4', 'wakilempat', '6ad14ba9986e3615423dfca256d04e3f', 'wakilempat@mail.com', 7, '', '089636555456', '', 1, 0, '2020-02-27 05:35:56', '0000-00-00 00:00:00', '', '', NULL);

SET FOREIGN_KEY_CHECKS = 1;
