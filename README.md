# Jawicode Framework 2 #

Build with Codeigniter 3.1.9 + AngularJs

### Features ###

* Auth
* Access Management
* CRUD Generator include template (localhost/jawicode2/crud)
* Suport master detail form
* Lookup data

### How do I get set up? ###

* copy paste project folder
* import DB (assets/db/)
* setup config.php, database.php 


### Contribution guidelines ###

* Free to use (with watermark on footer page)

### Thanks for contributing ###

